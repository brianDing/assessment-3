#!/bin/bash

if (( $#  > 0 ))
then
    hostname=$1
else
    echo "WTF: you must supply a hostname or IP address" 1>&2 
    exit 1 
fi 

ssh -o StrictHostKeyChecking=no -i ~/.ssh/BTKEY.pem ec2-user@$hostname '

#Install git

sudo yum install git -y
yes | git clone git@bitbucket.org:brianDing/assessment-3.git


#Install Terraform

sudo yum install -y yum-utils
sudo yum-config-manager --add-repo https://rpm.releases.hashicorp.com/AmazonLinux/hashicorp.repo
sudo yum -y install terraform

cd assessment-3

git checkout dev-secret-test

terraform init
terraform apply -target=module.networking -auto-approve

git add .
git commit -m "vpc and jenkins created"
git push origin dev-secret-test
'

## Need to add IAM role to instance for credentials
## Change origins for other branch to be used