#Calling modules and passing variables

module "networking" {
  source = "./modules/networking_tf"
  # Pass some variables
  key_name = var.key_name
  public_key_path = var.public_key_path
  environment_tag = var.environment_tag
  region = var.region
}

module "petclinic" {
  source = "./modules/petclinic_tf"
  # Pass some variables

  vpc_id = module.networking.vpc_id
  public_key_path = var.public_key_path
  environment_tag = var.environment_tag
  region = var.region
  sg_jenkins_master_id = module.networking.sg_jenkins_master_id
  sg_jenkins_worker_id = module.networking.sg_jenkins_worker_id
  public_subnet_1a_id = module.networking.public_subnet_1a_id
  public_subnet_1b_id = module.networking.public_subnet_1b_id
  public_subnet_1c_id = module.networking.public_subnet_1c_id
  web_ami = "ami-063d4ab14480ac177"
  key_name = var.key_name
  db_name = var.db_name
  db_user = var.db_user
  db_master_pass = module.db.db_master_pass
  db_host = module.db.db_host
}

module "db" {
  source = "./modules/db_tf"

  vpc_id = module.networking.vpc_id
  environment_tag = var.environment_tag
  sg_jenkins_master_id = module.networking.sg_jenkins_master_id
  sg_jenkins_worker_id = module.networking.sg_jenkins_worker_id
  private_subnet_id_1a = module.networking.private_subnet_id_1a
  private_subnet_id_1b = module.networking.private_subnet_id_1b
  private_subnet_id_1c = module.networking.private_subnet_id_1c
  web_sg = module.petclinic.web_sg
  db_user = var.db_user
  db_name = var.db_name
  web_sg_dev = module.dev.web_sg_dev
}


module "secrets" {
  source = "./modules/secret_tf"

  vpc_id = module.networking.vpc_id

  environment_tag = var.environment_tag
  db_sg = module.db.db_sg
  db_subnet_group = module.db.db_subnet_group
  db_user = var.db_user
  db_name = var.db_name
  db_id = module.db.db_id
}

module "secrets_refresh" {
  source = "./modules/secret_tf_refresh"

  vpc_id = module.networking.vpc_id

  environment_tag = var.environment_tag
  db_sg = module.db.db_sg
  db_subnet_group = module.db.db_subnet_group
  db_user = var.db_user
  db_name = var.db_name
  db_id = module.db.db_id
}


module "dev"{
  source = "./modules/dev_env"
  
  key_name = var.key_name
  public_subnet_1a_id = module.networking.public_subnet_1a_id
  environment_tag = var.environment_tag
  vpc_id = module.networking.vpc_id
  db_user = var.db_user
  db_master_pass = module.db.db_master_pass
  db_host = module.db.db_host
}

# Setting up key pair path

resource "aws_key_pair" "ec2key" {
  key_name = "BTKEY"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCgtW6mOA9ykq+cq5ovOtMQqVVavRp7WlQr27PQpcg7Uvm/cXB0yk8fPoh+5Nl0R9FN4QPwPmD3f/gb6xDMXIJ6w94z4VnP6hLWJdi8hc5wwSR+Oskjxu2EfzzQI64kzS+jUFSl81Rqdv7G8o/Vooo3s0G0x8cKdGsbocLXIKA338yxnic/Zz/HBVqTe/u1BYeNWUeR1/RS5qs5IYXzBX7Q3SyqjMaBBRWRQVhWWvG7WnIXmtM0ID5VVhBFt2G1EV4orgnJ+OdZyA1buYgUshrfs/TzlSwLjuWImOCnieqh/qYD6a0BxnhC7Wyy31bibfiDlxivP/579dUfVId36TVh BTKEY"
}
