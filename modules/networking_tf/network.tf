/* -------------------------------------------------------------------------- */
/*                         Setting up VPC & Networking                        */
/* -------------------------------------------------------------------------- */


/* -------------------------------------------------------------------------- */
/*                              Setting up a VPC                              */
/* -------------------------------------------------------------------------- */

resource "aws_vpc" "vpc" {
  cidr_block = "90.123.0.0/16"
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags = {
    "Name" = "bt-vpc"
    "Environment" = "${var.environment_tag}"
  }
}

/* -------------------------------------------------------------------------- */
/*                       Setting up an Internet Gateway                       */
/* -------------------------------------------------------------------------- */

resource "aws_internet_gateway" "igw" {
  vpc_id = "${aws_vpc.vpc.id}"
  tags = {
    "Environment" = "${var.environment_tag}"
    "Name" = "bt-igw"
  }
}

/* -------------------------------------------------------------------------- */
/*               Subnets, Route Tables & Route Table Associations             */
/* -------------------------------------------------------------------------- */
resource "aws_subnet" "subnet_public_1a" {
  vpc_id = "${aws_vpc.vpc.id}"
  cidr_block = "90.123.1.0/24"
  map_public_ip_on_launch = true
  availability_zone = "eu-west-1a"
  tags = {
    "Environment" = "${var.environment_tag}"
    "Type" = "Public"
    "Name" = "${var.environment_tag}-public-subnet-1a"
  }
}

resource "aws_subnet" "subnet_public_1b" {
  vpc_id = "${aws_vpc.vpc.id}"
  cidr_block = "90.123.3.0/24"
  map_public_ip_on_launch = true
  availability_zone = "eu-west-1b"
  tags = {
    "Environment" = "${var.environment_tag}"
    "Type" = "Public"
    "Name" = "${var.environment_tag}-public-subnet-1b"
  }
}

resource "aws_subnet" "subnet_public_1c" {
  vpc_id = "${aws_vpc.vpc.id}"
  cidr_block = "90.123.5.0/24"
  map_public_ip_on_launch = true
  availability_zone = "eu-west-1c"
  tags = {
    "Environment" = "${var.environment_tag}"
    "Type" = "Public"
    "Name" = "${var.environment_tag}-public-subnet-1c"
  }
}

resource "aws_route_table" "rtb_public" {
  vpc_id = "${aws_vpc.vpc.id}"
route {
      cidr_block = "0.0.0.0/0"
      gateway_id = "${aws_internet_gateway.igw.id}"
  }
tags = {
    "Name"        = "${var.environment_tag}-public-route-table"
    "Environment" = "${var.environment_tag}"
  }
}

resource "aws_route_table_association" "rta_subnet_public_1a" {
  subnet_id      = "${aws_subnet.subnet_public_1a.id}"
  route_table_id = "${aws_route_table.rtb_public.id}"
}

resource "aws_route_table_association" "rta_subnet_public_1b" {
  subnet_id      = "${aws_subnet.subnet_public_1b.id}"
  route_table_id = "${aws_route_table.rtb_public.id}"
}

resource "aws_route_table_association" "rta_subnet_public_1c" {
  subnet_id      = "${aws_subnet.subnet_public_1c.id}"
  route_table_id = "${aws_route_table.rtb_public.id}"
}

resource "aws_subnet" "subnet_private_1a" {
  vpc_id = "${aws_vpc.vpc.id}"
  cidr_block = "90.123.0.0/24"
  map_public_ip_on_launch = false
  availability_zone = "eu-west-1a"
  tags = {
    "Environment" = "${var.environment_tag}"
    "Type" = "Private"
    "Name" = "${var.environment_tag}-private-subnet-1a"
  }
}

resource "aws_subnet" "subnet_private_1b" {
  vpc_id = "${aws_vpc.vpc.id}"
  cidr_block = "90.123.2.0/24"
  map_public_ip_on_launch = false
  availability_zone = "eu-west-1b"
  tags = {
    "Environment" = "${var.environment_tag}"
    "Type" = "Private"
    "Name" = "${var.environment_tag}-private-subnet-1b"
  }
}

resource "aws_subnet" "subnet_private_1c" {
  vpc_id = "${aws_vpc.vpc.id}"
  cidr_block = "90.123.4.0/24"
  map_public_ip_on_launch = false
  availability_zone = "eu-west-1c"
  tags = {
    "Environment" = "${var.environment_tag}"
    "Type" = "Private"
    "Name" = "${var.environment_tag}-private-subnet-1c"
  }
}

resource "aws_route_table" "rtb_private" {
  vpc_id = "${aws_vpc.vpc.id}"

  tags = {
    "Name"        = "${var.environment_tag}-private-route-table"
    "Environment" = "${var.environment_tag}"
  }
}

resource "aws_route_table_association" "rta_subnet_private_1a" {
  subnet_id      = "${aws_subnet.subnet_private_1a.id}"
  route_table_id = "${aws_route_table.rtb_private.id}"
}

resource "aws_route_table_association" "rta_subnet_private_1b" {
  subnet_id      = "${aws_subnet.subnet_private_1b.id}"
  route_table_id = "${aws_route_table.rtb_private.id}"
}

resource "aws_route_table_association" "rta_subnet_private_1c" {
  subnet_id      = "${aws_subnet.subnet_private_1c.id}"
  route_table_id = "${aws_route_table.rtb_private.id}"
}

resource "aws_main_route_table_association" "rta_private" {
  vpc_id         = "${aws_vpc.vpc.id}"
  route_table_id = "${aws_route_table.rtb_private.id}"
}


/* -------------------------------------------------------------------------- */
/*                                    NACLs                                   */
/* -------------------------------------------------------------------------- */

resource "aws_network_acl" "public-nacl" {
  vpc_id = "${aws_vpc.vpc.id}"
  subnet_ids = [ "${aws_subnet.subnet_public_1a.id}", aws_subnet.subnet_public_1b.id, aws_subnet.subnet_public_1c.id ]
  egress {
    protocol   = -1
    rule_no    = 101
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    icmp_code  = 0
    icmp_type  = 0
    from_port  = 0
    to_port    = 0
  }
  ingress {
    protocol   = -1
    rule_no    = 101
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    icmp_code  = 0
    icmp_type  = 0
    from_port  = 0
    to_port    = 0
  }
  tags = {
    "Name" = "${var.environment_tag}-public-nacl"
  }
}

resource "aws_default_network_acl" "default" {
  default_network_acl_id = "${aws_vpc.vpc.default_network_acl_id}"
  subnet_ids = [ "${aws_subnet.subnet_private_1a.id}", "${aws_subnet.subnet_private_1b.id}" , "${aws_subnet.subnet_private_1c.id}" ]
  lifecycle {
    ignore_changes = [subnet_ids]
  }
  ingress {
    protocol   = -1
    rule_no    = 100
    action     = "allow"
    cidr_block = "${aws_vpc.vpc.cidr_block}"
    from_port  = 0
    to_port    = 0
  }

  egress {
    protocol   = -1
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }
  tags = {
    "Name"        = "${var.environment_tag}-private-nacl"
  }
}

