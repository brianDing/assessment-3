output "vpc_id" {
    description = "this is the vpc id"
    value = aws_vpc.vpc.id
}

output "private_subnet_id_1a" {
    description = "this is the private subnet id"
    value = aws_subnet.subnet_private_1a.id
}

output "private_subnet_id_1b" {
    description = "this is the private subnet id"
    value = aws_subnet.subnet_private_1b.id
}

output "private_subnet_id_1c" {
    description = "this is the private subnet id"
    value = aws_subnet.subnet_private_1c.id
}


output "sg_jenkins_master_id" {
    description = "this is the jenkins master sg id"
    value = aws_security_group.sg_jenkins_master.id
}

output "sg_jenkins_worker_id" {
    description = "this is the jenkins worker sg id"
    value = aws_security_group.sg_jenkins_worker.id
}


output "public_subnet_1a_id" {
    description = "this is the public subnet id for eu-west-1a"
    value = aws_subnet.subnet_public_1a.id
  
}

output "public_subnet_1b_id" {
    description = "this is the public subnet id for eu-west-1b"
    value = aws_subnet.subnet_public_1b.id
  
}

output "public_subnet_1c_id" {
    description = "this is the public subnet id for eu-west-1c"
    value = aws_subnet.subnet_public_1c.id
  
}