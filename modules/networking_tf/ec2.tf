/* -------------------------------------------------------------------------- */
/*                   Jenkins Master & Worker Security Groups                  */
/* -------------------------------------------------------------------------- */

resource "aws_security_group" "sg_jenkins_master" {
  name = "sg_${var.environment_tag}_jenkins_master"
  vpc_id = "${aws_vpc.vpc.id}"
  description = "Allow access from home IPs"
  ingress {
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
      cidr_blocks = ["90.212.182.216/32", "82.33.58.195/32"]
  }
egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
tags = {
    "Environment" = "${var.environment_tag}"
    "Name"        = "sg-${var.environment_tag}-jenkins_master"
  }
}

resource "aws_security_group" "sg_jenkins_worker" {
  name = "sg_${var.environment_tag}_jenkins_worker"
  vpc_id = "${aws_vpc.vpc.id}"
  description = "Allow access from home IPs and Jenkins IPs"
  ingress {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      security_groups = [aws_security_group.sg_jenkins_master.id]
  }
egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
tags = {
    "Environment" = "${var.environment_tag}"
    "Name"        = "sg-${var.environment_tag}-jenkins_worker"
  }
}

/* -------------------------------------------------------------------------- */
/*                    AMI Data for Jenkins master & worker                    */
/* -------------------------------------------------------------------------- */

data "aws_ami" "jenkins_master_ami" {
  most_recent = true
  owners = ["self"]
  filter {                       
    name = "tag:Name"     
    values = ["bt-jenkins-master"]
  }                              
}

data "aws_ami" "jenkins_worker_ami" {
  most_recent = true
  owners = ["self"]
  filter {                       
    name = "tag:Name"     
    values = ["bt-jenkins-worker"]
  }                              
}

/* -------------------------------------------------------------------------- */
/*                    Jenkins Master and 2 Worker instances                   */
/* -------------------------------------------------------------------------- */

resource "aws_instance" "jenkins_master" {
  ami = data.aws_ami.jenkins_master_ami.image_id
  instance_type = "t2.micro"
  availability_zone = "eu-west-1a"
  associate_public_ip_address = true
  subnet_id = aws_subnet.subnet_public_1a.id
  security_groups = [aws_security_group.sg_jenkins_master.id, aws_security_group.sg_bitbucket.id]
  key_name = var.key_name
  user_data = "${base64encode(<<-EOF
            #!/bin/bash

            #Install git
            sudo yum install git -y
            yes | git clone https://Tanya-Moller@bitbucket.org/brianDing/assessment-3.git
            
            #Install Terraform & Packer
            sudo yum install -y yum-utils
            sudo yum-config-manager --add-repo https://rpm.releases.hashicorp.com/AmazonLinux/hashicorp.repo
            sudo yum -y install terraform
            sudo yum -y install packer

            #Install Apache and create reverse proxy
            sudo yum install -y httpd
            sudo systemctl start httpd
            cd /etc/httpd/conf.d
            sudo sh -c "echo \"<VirtualHost *:80>
            ProxyPreserveHost On

            ProxyPass / http://127.0.0.1:8080/
            ProxyPassReverse / http://127.0.0.1:8080/
            </VirtualHost>\" > jenkins.conf"
            sudo systemctl restart httpd
            sudo systemctl enable httpd

            EOF
  )}"
  tags = {
      "Name" = "bt-jenkins-master"
  }
}

resource "aws_instance" "jenkins_worker" {
  ami = data.aws_ami.jenkins_worker_ami.image_id
  count = 2
  instance_type = "t2.micro"
  availability_zone = "eu-west-1a"
  associate_public_ip_address = true
  subnet_id = aws_subnet.subnet_public_1a.id
  security_groups = [aws_security_group.sg_jenkins_worker.id, aws_security_group.sg_bitbucket.id]
  iam_instance_profile = "ALAcademyJenkinsSuperRole"
  key_name = var.key_name
    user_data = "${base64encode(<<-EOF
            #!/bin/bash

            #Install git
            sudo yum install git -y
            yes | git clone https://Tanya-Moller@bitbucket.org/brianDing/assessment-3.git
            
            #Install Terraform & Packer
            sudo yum install -y yum-utils
            sudo yum-config-manager --add-repo https://rpm.releases.hashicorp.com/AmazonLinux/hashicorp.repo
            sudo yum -y install terraform
            sudo yum -y install packer

            #Install Ansible
            sudo amazon-linux-extras install ansible2

            EOF
  )}"
  tags = {
      "Name" = "bt-jenkins-worker-${count.index}"
  }
}

/* -------------------------------------------------------------------------- */
/*                                  DNS Name                                  */
/* -------------------------------------------------------------------------- */

resource "aws_route53_record" "jenkins" {
  zone_id = "Z03386713MCJ0LHJCA6DL"
  name    = "bt-jenkins.academy.labs.automationlogic.com"
  type    = "A"
  ttl     = "300"
  records = [aws_instance.jenkins_master.public_ip]
}

/* -------------------------------------------------------------------------- */
/*                    Webhook (BitBucket IP) Security Group                   */
/* -------------------------------------------------------------------------- */

resource "aws_security_group" "sg_bitbucket" {
  name = "sg_${var.environment_tag}_bitbucket"
  vpc_id = "${aws_vpc.vpc.id}"
  description = "Allow access from BitBucket IPs"
  ingress {
      from_port   = 8080
      to_port     = 8080
      protocol    = "tcp"
      cidr_blocks = ["13.52.5.96/28", "13.236.8.224/28", "18.136.214.96/28", "18.184.99.224/28", "18.234.32.224/28", "18.246.31.224/28", "52.215.192.224/28", "104.192.137.240/28", "104.192.138.240/28", "104.192.140.240/28", "104.192.142.240/28", "104.192.143.240/28", "185.166.143.240/28", "185.166.142.240/28"]
  }
egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
tags = {
    "Environment" = "${var.environment_tag}"
    "Name"        = "sg-${var.environment_tag}-bitbucket"
  }
}