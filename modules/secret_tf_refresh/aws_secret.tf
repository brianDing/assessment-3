resource "random_password" "db_master_pass" {
  length            = 30
  special           = true
  min_special       = 4
  override_special  = "?"
  keepers           = {
    pass_version  = 1
  }
}



resource "local_file" "db_pw2" {
    content     = random_password.db_master_pass.result
    filename = "/home/ec2-user/db_pw2"
}
