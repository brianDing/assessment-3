/* -------------------------------------------------------------------------- */
/*                                Webserver SG                                */
/* -------------------------------------------------------------------------- */

resource "aws_security_group" "sg_web" {
  name = "sg_${var.environment_tag}_webserver"
  vpc_id = var.vpc_id
  description = "Allow 8080 from lb and SSH from Jenkins"
  ingress {
      from_port   = 8080
      to_port     = 8080     
      protocol    = "tcp"
      security_groups = [aws_security_group.sg_lb.id]
  }
  ingress {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      security_groups = [var.sg_jenkins_master_id, var.sg_jenkins_worker_id, aws_security_group.sg_lb.id]
  }
  egress {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = ["0.0.0.0/0"]
    }
  tags = {
      "Environment" = "${var.environment_tag}"
      "Name"        = "sg-${var.environment_tag}-webserver"
    }
}

/* -------------------------------------------------------------------------- */
/*                             Petclinic AMI data                             */
/* -------------------------------------------------------------------------- */

data "aws_ami" "petclinic_ami" {
  most_recent = true
  owners = ["self"]
  filter {                       
    name = "tag:Name"     
    values = ["petclinic-ami"]
  }                              
}

/* -------------------------------------------------------------------------- */
/*                  Launch Template for PetClinic Webservers                  */
/* -------------------------------------------------------------------------- */
# data "template_file" "webinstance" {
#   template = "/modules/petclinic/web.tpl"
#   vars = {
#     db_host = var.db_host
#     db_user = var.db_user
#     db_pw = var.db_master_pass
#   }
# }

resource "aws_launch_template" "default" {
  name = "bt_webserver_lt"
  instance_type = var.instance_type
  image_id        = data.aws_ami.petclinic_ami.image_id
  vpc_security_group_ids = [aws_security_group.sg_web.id]
  key_name = var.key_name
  

  tags = {
      Name = "webserver"
  }
  user_data = base64encode(templatefile(".//modules/petclinic_tf/web2.tpl", 
  # user_data = filebase64("modules/petclinic_tf/web2.tpl",
  {
    db_host = "${var.db_host}"
    db_user = "${var.db_user}"
    db_pw = "${var.db_master_pass}"

  }
  ))
  # user_data = filebase64(data.template_file.webinstance.rendered)
  # "${data.template_file.webinstance.rendered}"
  
  
# user_data = "${base64encode(<<-EOF
# #!/bin/bash

# cd ~/petclinic/
# touch test.txt
# sudo sed -i "s,spring.datasource.url=jdbc:mysql://localhost/petclinic,spring.datasource.url=jdbc:mysql://${db_host}/petclinic," ~/petclinic/src/main/resources/application.properties
# sudo sed -i "s,spring.datasource.username=petclinic,spring.datasource.username=${db_user}," ~/petclinic/src/main/resources/application.properties
# sudo sed -i "s,spring.datasource.password=petclinic,spring.datasource.password=${db_pw}," ~/petclinic/src/main/resources/application.properties

# sudo mvn -f /home/ec2-user/petclinic/pom.xml -Dmaven.test.skip=true package

# mysql -h ${db_host} -u ${db_user} --password="${db_pw}" < /home/ec2-user/petclinic/src/main/resources/db/mysql/schema.sql
# mysql -h ${db_host} -u ${db_user} --password="${db_pw}" < /home/ec2-user/petclinic/src/main/resources/db/mysql/data.sql
# mysql -h ${db_host} -u ${db_user} --password="${db_pw}" < /home/ec2-user/create_user.sql

# sudo chkconfig --add petclinic
# sudo systemctl enable petclinic

# sudo systemctl start httpd
# sudo /etc/init.d/petclinic stop >/dev/null 2>&1 
# sudo /etc/init.d/petclinic start >/dev/null 2>&1 
# sleep 20
#             EOF
#   )}"
}

/* -------------------------------------------------------------------------- */
/*                    Autoscaling Group for Load balancing                    */
/* -------------------------------------------------------------------------- */

resource "aws_autoscaling_group" "default" {
  vpc_zone_identifier = [ var.public_subnet_1a_id, var.public_subnet_1b_id, var.public_subnet_1c_id ]
  name = "bt_autoscaling-group"
  min_size = 3
  max_size = 10
  force_delete = true

  target_group_arns = [ aws_lb_target_group.default.arn ]
  health_check_type = "EC2"

  instance_refresh {
    strategy = "Rolling"
    triggers = ["launch_template", "tag"]
    preferences {
      instance_warmup = 300
      min_healthy_percentage = 30
    }
  }
  tag {
    key                 = "AMI-ID"
    value               = data.aws_ami.petclinic_ami.image_id
    propagate_at_launch = false
  }
  launch_template {
    id      = aws_launch_template.default.id
    version = "$Latest"
  }
  lifecycle {
    ignore_changes = [load_balancers, target_group_arns]
    create_before_destroy = true
  }

  # The "ELB" health check is much more robust, as it tells the ASG to use the CLB’s health check to determine if an Instance is healthy or not and to automatically replace Instances if the CLB reports them as unhealthy. That way, Instances will be replaced not only if they are completely down, but also if, for example, they’ve stopped serving requests because they ran out of memory or a critical process crashed.

  tag {
    key                 = "Name"
    value               = "bt_asg"
    propagate_at_launch = true
  }
} 

/* -------------------------------------------------------------------------- */
/*            Autoscaling Policy for Scale up and Scale Down                  */
/* -------------------------------------------------------------------------- */

resource "aws_autoscaling_policy" "agents-scale-up" {
    name = "agents-scale-up"
    scaling_adjustment = 1
    adjustment_type = "ChangeInCapacity"
    cooldown = 300
    autoscaling_group_name = "${aws_autoscaling_group.default.name}"
}

resource "aws_autoscaling_policy" "agents-scale-down" {
    name = "agents-scale-down"
    scaling_adjustment = -1
    adjustment_type = "ChangeInCapacity"
    cooldown = 300
    autoscaling_group_name = "${aws_autoscaling_group.default.name}"
}

/* -------------------------------------------------------------------------- */
/*                    CloudWatch Metric Alarms for Scaling                    */
/* -------------------------------------------------------------------------- */

resource "aws_cloudwatch_metric_alarm" "memory-high" {
    alarm_name = "mem-util-high-agents"
    comparison_operator = "GreaterThanOrEqualToThreshold"
    evaluation_periods = "2"
    metric_name = "MemoryUtilization"
    namespace = "System/Linux"
    period = "300"
    statistic = "Average"
    threshold = "80"
    alarm_description = "This metric monitors ec2 memory for high utilization on agent hosts"
    alarm_actions = [
        "${aws_autoscaling_policy.agents-scale-up.arn}"
    ]
    dimensions = {
        AutoScalingGroupName = "${aws_autoscaling_group.default.name}"
    }
}

resource "aws_cloudwatch_metric_alarm" "memory-low" {
    alarm_name = "mem-util-low-agents"
    comparison_operator = "LessThanOrEqualToThreshold"
    evaluation_periods = "2"
    metric_name = "MemoryUtilization"
    namespace = "System/Linux"
    period = "300"
    statistic = "Average"
    threshold = "40"
    alarm_description = "This metric monitors ec2 memory for low utilization on agent hosts"
    alarm_actions = [
        "${aws_autoscaling_policy.agents-scale-down.arn}"
    ]
    dimensions = {
        AutoScalingGroupName = "${aws_autoscaling_group.default.name}"
    }
}

/* -------------------------------------------------------------------------- */
/*                                Load Balancer                               */
/* -------------------------------------------------------------------------- */

resource "aws_lb" "default" {
  name               = "bt-loadbalancer"
  internal           = false
  load_balancer_type = "application"
  # availability_zones = data.aws_availability_zones.all.names
  security_groups = [ aws_security_group.sg_lb.id ]
  subnets = [ var.public_subnet_1a_id, var.public_subnet_1b_id, var.public_subnet_1c_id ]

  tags = {
      Name = "bt_lb"
    }
}

/* -------------------------------------------------------------------------- */
/*                                Target Group                                */
/* -------------------------------------------------------------------------- */

resource "aws_lb_target_group" "default" {
  name     = "bt-lb-tg"
  port     = 8080
  protocol = "HTTP"
  target_type = "instance"
  vpc_id   = var.vpc_id
    health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    interval            = 30
    }
}

/* -------------------------------------------------------------------------- */
/*                         Listener for Load Balancer                         */
/* -------------------------------------------------------------------------- */

resource "aws_lb_listener" "default" {
  load_balancer_arn = aws_lb.default.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "forward"
    target_group_arn = aws_lb_target_group.default.arn
  }
}

/* -------------------------------------------------------------------------- */
/*                           Autoscaling Attachment                           */
/* -------------------------------------------------------------------------- */

resource "aws_autoscaling_attachment" "asg_attachment_bt" {
  autoscaling_group_name = aws_autoscaling_group.default.id
  alb_target_group_arn = aws_lb_target_group.default.arn
}

/* -------------------------------------------------------------------------- */
/*                         Loadbalancer Security Group                        */
/* -------------------------------------------------------------------------- */

resource "aws_security_group" "sg_lb" {
  name = "sg_${var.environment_tag}_loadbalancer"
  vpc_id = var.vpc_id
  description = "Allow access from all and SSH from Jenkins"
  ingress {
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
  }
  ingress {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      security_groups = [var.sg_jenkins_master_id, var.sg_jenkins_worker_id]
  }
egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
tags = {
    "Environment" = "${var.environment_tag}"
    "Name"        = "sg-${var.environment_tag}-loadbalancer"
  }
}



/* -------------------------------------------------------------------------- */
/*                             DNS Name (Route 53)                            */
/* -------------------------------------------------------------------------- */

resource "aws_route53_record" "lb" {
  zone_id = "Z03386713MCJ0LHJCA6DL"
  name    = "bt-petclinic.academy.labs.automationlogic.com"
  type    = "A"

  alias {
    name                   = aws_lb.default.dns_name
    zone_id                = aws_lb.default.zone_id
    evaluate_target_health = true
  }
}
