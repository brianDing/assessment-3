#cloud-boothook
#!/bin/bash

cd home/ec2-user/petclinic/
sudo touch test.txt
# sudo sed -i "s,spring.datasource.url=jdbc:mysql://localhost/petclinic,spring.datasource.url=jdbc:mysql://${db_host}/petclinic," ~/petclinic/src/main/resources/application.properties
# sudo sed -i "s,spring.datasource.username=petclinic,spring.datasource.username=${db_user}," ~/petclinic/src/main/resources/application.properties
# sudo sed -i "s,spring.datasource.password=petclinic,spring.datasource.password=${db_pw}," ~/petclinic/src/main/resources/application.properties

# sudo mvn -f /home/ec2-user/petclinic/pom.xml -Dmaven.test.skip=true package

mysql -h ${db_host} -u ${db_user} --password="${db_pw}" < /home/ec2-user/petclinic/src/main/resources/db/mysql/schema.sql
mysql -h ${db_host} -u ${db_user} --password="${db_pw}" < /home/ec2-user/petclinic/src/main/resources/db/mysql/data.sql
mysql -h ${db_host} -u ${db_user} --password="${db_pw}" < /home/ec2-user/create_user.sql

sudo chkconfig --add petclinic
sudo systemctl enable petclinic

sudo systemctl start httpd
sudo /etc/init.d/petclinic stop >/dev/null 2>&1 
sudo /etc/init.d/petclinic start >/dev/null 2>&1 
sleep 20
