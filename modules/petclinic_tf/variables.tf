# Defining variables

variable "region" {

}

variable "environment_tag" {

}

variable "public_key_path" {

}

variable "db_name" {

}

variable "db_user" {

}

variable "vpc_id" {

}

variable "sg_jenkins_master_id" {

}

variable "sg_jenkins_worker_id" {

}
variable "base_ami" {
    description = "Amazon Linux AMI"
    default = "ami-063d4ab14480ac177"
}

variable "instance_type" {
    description = "Default instance type"
    default = "t2.micro"
}

variable "zone_A"{
    description = "Availablility zone A"
    default = "eu-west-1a"
}

variable "zone_B"{
    description = "Availablility zone b"
    default = "eu-west-1b"
}

variable "public_subnet_1a_id" {
  
}

variable "public_subnet_1b_id" {
  
}

variable "public_subnet_1c_id" {
  
}

variable "web_ami" {

}

variable "key_name" {

} 

variable "db_master_pass" {
  
}

variable "db_host" {
  
}