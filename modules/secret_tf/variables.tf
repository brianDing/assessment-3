variable "vpc_id" {
  
}

variable "environment_tag" {
  
}

variable "db_name" {
  
}

variable "db_user" {
  
}

variable "db_sg" {
  
}

variable "db_subnet_group" {

}

variable "db_id" {
  
}