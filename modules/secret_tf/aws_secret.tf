# resource "random_password" "db_master_pass" {
#   length            = 30
#   special           = true
#   min_special       = 5
#   override_special  = "#^&()=+"
#   keepers           = {
#     pass_version  = 1
#   }
# }

# resource "local_file" "db_pw2" {
#     content     = random_password.db_master_pass.result
#     filename = "/home/ec2-user/db_pw"
# }

# resource "random_id" "pass" {
#   keepers = {
#     # Generate a new id each time we switch to a new AMI id
#     rd_pwd = "${random_password.db_master_pass.result}"
#   }

#   byte_length = 8
# }


# resource "aws_secretsmanager_secret" "db-pass" {
#   name = "db-pass-${var.environment_tag}-${random_id.pass.hex}"
#   tags = {
#     "Name" = "test1"
#   }
# }

# resource "aws_secretsmanager_secret_version" "db-pass-val" {
#   secret_id     = aws_secretsmanager_secret.db-pass.id
#   secret_string = random_password.db_master_pass.result
# }