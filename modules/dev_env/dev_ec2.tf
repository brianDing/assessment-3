data "aws_ami" "petclinic_ami" {
  most_recent = true
  owners = ["self"]
  filter {                       
    name = "tag:Name"     
    values = ["petclinic-ami"]
  }                              
}

resource "aws_security_group" "sg_web_dev" {
  name = "sg_${var.environment_tag}_webserver-dev"
  vpc_id = var.vpc_id
  description = "Allow 8080 from lb and SSH from Jenkins"
  ingress {
      from_port   = 8080
      to_port     = 8080     
      protocol    = "tcp"
      cidr_blocks = ["82.33.58.195/32", "90.212.182.216/32"]
  }
  ingress {
      from_port   = 80
      to_port     = 80     
      protocol    = "tcp"
      cidr_blocks = ["82.33.58.195/32", "90.212.182.216/32"]
  }
  ingress {
      from_port   = 22
      to_port     = 22     
      protocol    = "tcp"
      cidr_blocks = ["82.33.58.195/32", "90.212.182.216/32"]
  }
  egress {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = ["0.0.0.0/0"]
    }
  tags = {
      "Environment" = "${var.environment_tag}"
      "Name"        = "sg-${var.environment_tag}-webserver-dev"
    }
}

resource "aws_instance" "dev_env"{
    ami = data.aws_ami.petclinic_ami.image_id
    instance_type = var.instance_type
    subnet_id = var.public_subnet_1a_id
    vpc_security_group_ids = [aws_security_group.sg_web_dev.id]
    key_name = var.key_name

    tags = { 
        Name = "bt-test-env"
    }

    # user_data = "${base64encode(<<-EOF
    #         #!/bin/bash
    #         cd ~/petclinic/
    #         sudo chkconfig --add petclinic
    #         sudo systemctl enable petclinic
    #         export db_host=bt-db.academy.labs.automationlogic.com
    #         export db_user=admin
    #         export db_pw=secret!123
    #         mysql -h $db_host -u $db_user --password="$db_pw" < /home/ec2-user/petclinic/src/main/resources/db/mysql/schema.sql
    #         mysql -h $db_host -u $db_user --password="$db_pw" < /home/ec2-user/petclinic/src/main/resources/db/mysql/data.sql
    #         mysql -h $db_host -u $db_user --password="$db_pw" < /home/ec2-user/create_user.sql
    #         sudo systemctl start httpd
    #         sudo /etc/init.d/petclinic stop >/dev/null 2>&1 
    #         sudo /etc/init.d/petclinic start >/dev/null 2>&1 
    #         sleep 20
    #         EOF
    # )}"

  user_data = base64encode(templatefile(".//modules/petclinic_tf/web2.tpl", {
    db_host = "${var.db_host}"
    db_user = "${var.db_user}"
    db_pw = "${var.db_master_pass}"

  }))
}

resource "aws_route53_record" "dev_IP" {
  zone_id = "Z03386713MCJ0LHJCA6DL"
  name    = "bt-petclinic-dev.academy.labs.automationlogic.com"
  type    = "A"
  ttl     = "300"
  records = [aws_instance.dev_env.public_ip]
}
