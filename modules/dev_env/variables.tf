# Defining variables
variable "base_ami" {
    description = "Amazon Linux AMI"
    default = "ami-063d4ab14480ac177"
}

variable "instance_type" {
    description = "Default instance type"
    default = "t2.micro"
}

variable "zone_A"{
    description = "Availablility zone A"
    default = "eu-west-1a"
}

variable "zone_B"{
    description = "Availablility zone b"
    default = "eu-west-1b"
}

variable "public_subnet_1a_id" {
  
}


variable "key_name" {

} 

variable "environment_tag"{
    
}

variable "vpc_id"{
    
}

variable "db_master_pass" {
  
}

variable "db_host" {
  
}

variable "db_user" {

}