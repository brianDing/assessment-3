output "db_id"{
    value = aws_db_instance.default.identifier
}

output "db_sg" {
  value = aws_security_group.sg_db.id
}

output "db_subnet_group" {
  value = aws_db_subnet_group.default.name
}

output "db_host" {
  value = aws_route53_record.database.name
}

output "db_master_pass" {
  value = aws_db_instance.default.password
}
