# Assessment 3 - ReadME

**This README contains a step-by-step guide to set up a VPC, a network inside it and how Jenkins that is already pre-configured manages CI/CD of the PetClinic application using cloud technology.**

The agreed client deliverables and user stories (used to prioritize workload and plan the project) can be found on Trello by following this link: 
- https://trello.com/b/CPNr2Nyr/assessment-3-ansible-terraform

<br>

# Part 1 - Setting up a VPC, Network & Automation Server (Jenkins) in one command
![picture](readme_diagrams/VPC_structure.png)

## Pre-configured Jenkins AMI

- Jenkins AMIs have already been created where the necessary packages and dependencies to support the pre-configured jobs are already installed
- SSH keys which are used to connect the Master and the Worker nodes as well as to connect to the BitBucket repo to use webhooks are already generated 
- The documentation for setting up a master and worker node for Jenkins can be found using the official Jenkins documentation.
  - https://www.jenkins.io/doc/book/installing/linux/
- Terraform has been set up to automatically find the most recent version of the Jenkins Master and Worker AMIs when being launched through `terraform apply`

<br>

## Single Command Setup
---
**The VPC, Network, Jenkins Master and 2 Worker nodes will be set up in a single command.**

### Prerequisites
- An EC2 machine running Amazon Linux with the ALJenkinsSuperRole IAM role and necessary security groups attached (The IP of the machine is used).
  

### *Do I need to install anything else?*
- The script initialize.sh installs Git, Terraform, clones the necessary repo into the machine and carries out a `terraform apply` on the `networking_tf` module.
- It then pushes to the repo to ensure the correct `terraform.tfstate` files are stored.
- All that needs to be done is running : 
  ```
  ./initialize.sh <public-ip>
  ```

<br>



## **Terraform Modules: Networking**
---
Inside `networking_tf` module:
- `ec2.tf` - Builds the Jenkins Host and Worker nodes, creates the route53 DNS and provisions the machines through user data  (e.g. reverse proxy for access on port 80, installing Ansible, Terraform, Git, Packer).
- `network.tf` - Builds the VPC and the network inluding NACLs, security groups, route tables and its associations, the Internet Gateway and subnets.
- `output.tf` and `variables.tf` to output and input variables between modules.


### **Resolving errors:**
- Ensure that the VPC and its networking (including Jenkins SGs and route53 records) are not already present in AWS under the same names.
- Ensure that the VM and BitBucket are set up correctly (SSH Keygen & Permissions) to allow cloning of and pushing to the repo.


<br>
<br>

## **Setting up Jenkins:**
---
 **A few minor adjustments need to be made before builds can be run on Jenkins.**

 
- Sign into the Jenkins GUI with the link below and update the worker nodes with their new private IPs. 
  - Select the worker (e.g. node-1)
  - Configure --> Under Launch method: change the Host to the Private IP of the first Jenkins worker.
  - Return to the worker (node-1) and press 'Trust SSH Host Key'.
  - Relaunch the agent and it should be online.
- Repeat this process for the second worker (node-2).
- Jenkins should now be ready to launch builds!
  
<br>

### **Resolving errors:**
- Ensure that the private SSH key within Jenkins matches the public SSH key in BitBucket to allow repo push/pull and use Webhook functionality.
- Ensure that the public SSH key of the master is in the `~/.ssh/authorized_keys` files of the workers.
- Ensure that the worker machines are running and the route53 record to the Master is set correctly.
- Ensure that the reverse proxy is not broken (Jenkins normally functions on port 8080) and that security permissions and IAM roles are set correctly.
- Ensure the VPC has not already been created

<br>

### LINK TO JENKINS:  http://bt-jenkins.academy.labs.automationlogic.com/
- default user: admin
- default password: adminpassword!123

<br>
<br>

# Part 2: Jenkins Automation of PetClinic
*How Jenkins is set up to build the necessary infrastructure to run PetClinic, taking into account the specifications required by the client.*

![picture](readme_diagrams/Jenkins_workflow.png)

<br>

## **Jenkins Pipeline**
---

- Clicking on the Production tab on the GUI will list out only production-related jobs, same for Dev.
- Both Dev and Production workflow uses Packer, Ansible, and Terraform.
- **TERRAFORM** modules used:
  - `secret_tf_refresh`  - Generate a random password for the database.
  - `db_tf` module - Setting up the database.
  - `petclinic_tf` module - Loadbalancing, Autoscaling (including ASG, Launch Template, Autoscaling Policies, Route53 records, and SGs).
- **[PACKER]** : `petclinic_ami` folder contains a Packer script in HCL format to create new Pet clinic AMIs.
- **[ANSIBLE]** : `ansible-slack` folder - Playbook that runs Slack notifications.
- *This setup will trigger a instance refresh for the Pet Clinic servers when a new AMI is built or when password refresh is triggered.*


<br>
<br>


## Jenkins Jobs: In Order of Build Sequence (Production)
- **For a fresh setup of Jenkins, run the production pipeline to create all the servers required for Pet Clinic in one go by running `1A_refresh_password`**
- `1A_refresh_password` is the start of a pipeline of jobs where the subsequent jobs are ran in a specific order to ensure they all have the correct database credentials 
- As the daily refresh affects both Production and Dev pipelines, the entire pipeline which is triggered automatically at 3am every day will run both pipelines to ensure the new database password is available in both environments

---

### **Step 1: Generating a Random Password for the database**

- **Name of Jenkins Job**: `1A_refresh_password`
- **Build Triggers**: Build Periodically -> Create a new password everyday at 3am (outside of working hours of 9am-6pm).
- **Build Environment**
  - Delete the workspace before build starts.
  - This ensures that no errors will occur wrt the `terraform.tfstate` files and that a new password is generated each time (not simply refreshing the state, hence keeping the same password).
- **Build**: Execute Shell

```bash
#!/bin/bash
rm /home/ec2-user/db_pw
rm /home/ec2-user/db_pw2
rm /home/ec2-user/db_user
rm /home/ec2-user/db_host
terraform init
terraform apply -target=module.secrets_refresh -auto-approve

cat /home/ec2-user/db_pw2
```
- Remove all database related files from the Jenkins worker home directory in preparation for the refresh
  - These files are kept on the worker so that other jobs can be run indepedently and still interact with the database
- Terraform runs the `secrets_refresh` module to generate a new random password and writes it to a file on the worker node home directory.
- `cat` is used here and in other jobs as a manual test to see, progressively, that the same password is being used. This can be removed for security reasons. 

**Post-build action:** Run Slack-notification job by passing a message as a parameter to run Ansible playbook

<br>

---

### **Step 2: Building an RDS Database**
- **Name of Jenkins Job**: `1B_rds`
- **Build Triggers** Build after other projects are built - `1A_refresh_password` (only if stable).
  - This ensures that the job is set to run automatically after the password is generated, which will use that password when setting up the database.
- **Build Environment**
  - Delete the workspace before build starts.
  - This ensures that no errors will occur with the `terraform.tfstate` files.
- **Build**: Execute Shell

```bash
#!/bin/bash
git checkout dev-secret-test

terraform init
terraform apply -target=module.db -auto-approve

cat /home/ec2-user/db_pw

git add .
git commit -m "db created/updated"
git push origin dev-secret-test
```

- Terraform runs the `db` module: `/modules/db_tf/db.tf` for the Terraform script.
- It creates a Database security group, a database subnet group and a Master/**Primary RDS** instance which calls in the password from the previously generated file.
- It is set to have backups between 9-10pm UTC.
- It is also available in more than one AZ and has a Route53 record.
- A **Replica RDS** is created, which acts as a failover, which in the case of the primary DB failing, will take over as the Primary.
- Lastly, 3 local files are created, each containing data about the db
  -  DB name (route53 record, `db_host`) 
  -  username (`db_user`)
  -   password (`db_pw`).
- These files are created for Packer to use when creating an AMI to ensure that the correct credentials are passed to the AMI so that they can connect with the database.
  - These files are deliberately created outside of the repo to ensure that the information is not pushed onto the repo nor accessible to the public.
- `cat` is again used here as a manual test to see, progressively, that the same password is being used. This can be removed for security reasons. 
- The workspace is then pushed to the repo to ensure that the `terraform.tfstate` files are updated and available for the next Terraform job.

**Post-build action:** Run Slack-notification job by passing a message as a parameter to run Ansible playbook


<br>

---

### **Step 3: Building a PetClinic AMI**

- **Name of Jenkins Job**: `2_create_petclinic_ami` 
- **Build Triggers**: Build after other projects are built - `1B_rds` (only if stable).
  - This ensures that the job is set to run automatically after the DB is created as PetClinic AMI needs the user, pwd and host data from the fresh RDS.

-  **Build**: Execute Shell

```bash
#!/bin/bash
db_host=$(cat /home/ec2-user/db_host)
db_user=$(cat /home/ec2-user/db_user)
db_pw=$(cat /home/ec2-user/db_pw)
sed -e "s;%HOST%;$db_host;g" -e "s;%USER%;$db_user;g" -e "s;%PW%;$db_pw;g" /home/ec2-user/workspace/2_create_petclinic_ami/petclinic_ami/prov2.tpl > /home/ec2-user/workspace/2_create_petclinic_ami/petclinic_ami/provision_install.sh
packer build /home/ec2-user/workspace/2_create_petclinic_ami/petclinic_ami/packer_create_AMI.pkr.hcl

```

- Each of the database credentials stored in the files are assigned to varaibles in the environment.
- Each of these declared variables are then used to replace the placeholders in the template file (.tpl) and then written into the bash script that the packer file uses to provision the Pet clinic AMI.
- By running `petclinic_ami/packer_create_AMI.pkr.hcl` it will create a new AMI following these steps:
  - Using the locals tag create a variable that holds the time that the script was run.
  - Pass that variable into the AMI name so that each AMI generated has a unique name.
  - Create an AMI using a Amazon Linux 2 base.
  - Send 3 files onto the AMI environment (provision_install.sh, create_user.sql, petclinic.init).
  - Provision the environment by running  `provision_install.sh` which will clone the Pet clinic repository, install Java and Maven required for the app, move and enable the init script and compile the app to create the executables required for the init script.

**Post-build action:** Run Slack-notification job by passing a message as a parameter to run Ansible playbook.

<br>

---
### **Step 4: Deploy Newest AMI, Build Loadbalancer, Set up Autoscaling**
- **Name of Jenkins Job**: `3_deploy_newest_AMI_PROD`
- **Build Triggers**: Build after other projects are built - `2_create_petclinic_ami` (only if stable).
  - This ensures that the job is set to run automatically after the new AMI is built.
- **Build Environment**
  - Delete the workspace before build starts.
  - This ensures that no errors will occur with the `terraform.tfstate` files.

- **Build**: Execute Shell
```bash
#!/bin/bash
git checkout dev-secret-test

terraform init
terraform apply -target=module.petclinic -auto-approve

git add .
git commit -m "new AMI deployed"
git push origin dev-secret-test
```

- Terraform runs the `petclinic` module: `/modules/petclinic_tf/autoscaling.tf` for the Terraform script.
- This module creates webservers in the form of Autoscaling Groups(ASG), the loadbalancer used for the instances in the ASGs, as well as the policies for when to scale up/down the ASG.
- By using the datasource tag, it will look for the latest Pet Clinic AMI and if there is a newer one than what the ASG is running it will trigger a refresh to update them with the new AMI.
- The ASG are set up so that they are in 3 AZs (1 in each) and has a Route53 record attached to the load balancer that is connected to all of them.
- An active decision has been made to set `create_before_destroy = true` for the ASG so that if an update was being implemented the servers won't be cut off causing down time but instead the load balancer will only be connected to the new ASGs once they are up and running and then delete the old machines
- The workspace is then pushed to the repo to ensure that the `terraform.tfstate` files are updated and available for the next Terraform job.

**Post-build action:** Run Slack-notification job by passing a message as a parameter to run Ansible playbook

<br>
<br>


## Jenkins Jobs: In Order of Build Sequence (Development)
---

### **Step 1: Using webhooks to generate a Development Pet Clinic AMI**

- **Name of Jenkins Job**: `DEV_create_petclinic_AMI`
- **Build Triggers**: Build when a change is pushed onto Bitbucket -> Webhook on branch dev-packer.
- **Build Environment**
  - Delete the workspace before build starts.
  - This ensures that no errors will occur with the `terraform.tfstate` files and that a new password is generated each time (not simply refreshing the state, hence keeping the same password).
- **Build**: Exactly the same as create AMI job in production
- **Post-build action:** Run Slack-notification job by passing a message as a parameter to run Ansible playbook
- *If the webhook does not work please refer to setting up Jenkins section above*

#### **Webhook Setup:**
- In Bitbucket, on the repo, select Webhooks in repo settings.
- The URL should have the syntax:
```
http://<jenkins-username>:<jenkins-password>@bt-jenkins.academy.labs.automationlogic.com:8080/bitbucket-hook/
```
- The trigger should be set to repository push.
- Ensure that the Jenkins machines have a security group attached with specific IPs for connecting to Bitbucket (see /modules/netowrking_tf/ec2.tf).


<br>

---
### **Step 2: Deploy development Pet Clinic AMI to Development environment**

- **Name of Jenkins Job**: `DEV_deploy_newest_AMI`
- **Build Triggers**: Build after other projects are built - `DEV_create_petclinic_ami` (only if stable).
- **Build Environment**
  - Delete the workspace before build starts.
  - This ensures that no errors will occur with the `terraform.tfstate` files and that a new password is generated each time (not simply refreshing the state, hence keeping the same password).
- **Build**: Execute Shell
```bash
#!/bin/bash
git checkout dev-secret-test
terraform init
terraform apply -target=module.dev -auto-approve
git add .
git commit -m "new DEV AMI deployed"
git push origin dev-secret-test
```
- Terraform runs the dev module: `/modules/dev_env` 
- This module creates a simple EC2 instance with the newly created development AMI with the purpose of seeing whether the changes in the new AMI works 
- bt-petclinic-dev.academy.labs.automationlogic.com:8080
- *There is a DNS name associated to the development server but will display the httpd default page instead of Petclinic*
- The HTTPD default page is used to check whether the server itself is up and running whilst the Pet Clinic app is spinning up which can take a few minutes.
- **Post-build action:** Run Slack-notification job by passing a message as a parameter to run Ansible playbook


