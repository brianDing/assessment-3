
output "db_user" {
    value = var.db_user
}

output "db_password" {
    value = var.db_user
}

output "db_name" {
    value = var.db_name
}

output "petclinic_ami_id"{
    value = module.petclinic.petclinic_ami_id
}
