variable "region" {
  description = "AWS Deployment region.."
  default = "eu-west-1"
}

variable "environment_tag" {
    description = "group name tag"
    default = "bt"
}

variable "public_key_path" {
    description = "This is the path to the ssh key"
    default = "~/.ssh/authorized_keys"
}

variable "key_name" {
 description = "SSH key to use"
 default = "BTKEY"
} 

variable "db_name" {
    description = "Name of the RDS DB"
    default = "petclinic_db"
}

variable "db_user" {
    description = "Username of the RDS DB"
    default = "admin"
}
